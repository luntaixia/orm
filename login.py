from configparser import ConfigParser

def get_credential(path = "/u/s1460341/keys/credentials-confidential.ini"):
    config = ConfigParser()
    config.read(path)
    username = config["Database"]["username"]
    password = config["Database"]["password"]
    return username, password


if __name__ == '__main__':
    get_credential()