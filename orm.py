import logging
import pandas as pd
import numpy as np
import contextlib

from sqlalchemy import create_engine, and_
from sqlalchemy.orm import sessionmaker, Session, Query
from sqlalchemy.inspection import inspect
from sqlalchemy.ext.declarative import declarative_base, as_declarative


class baseDbInf:
    def __init__(self, driver: str):
        self.driver = driver

    def bindServer(self, ip: str, port: int, db: str):
        self.ip = ip
        self.port = port
        self.db = db

    def login(self, username: str, password: str):
        self.username = username
        self.password = password

    def argTempStr(self):
        raise NotImplementedError("Must implement this method to get different arg placeholder for different database")

    def getConnStr(self) -> str:
        #engine = create_engine('mysql+mysqlconnector://USRNAME:PSWD@localhost:3306/DATABASE?charset=ytf8')
        #engine = create_engine("ibm_db_sa://USRNAME:PSWD@IP:PORT/DATABASE?charset=utf8")
        #engine = create_engine('sqlite:///DB_ADDRESS')
        return f"{self.driver}://{self.username}:{self.password}@{self.ip}:{self.port}/{self.db}?charset=utf8"

class DB2(baseDbInf):
    def __init__(self):
        super().__init__("ibm_db_sa")

    def argTempStr(self):
        return "?"

class SqlLite(baseDbInf):
    def __init__(self, dburl):
        super().__init__("sqlite")
        self.dburl = dburl

    def getConnStr(self) -> str:
        return f"sqlite:///{self.dburl}"

    def argTempStr(self):
        return "?"

class MySQL(baseDbInf):
    def __init__(self, driver = "mysqlconnector"):
        super().__init__(f"mysql+{driver}")

    def argTempStr(self):
        return "%s"


class LocalMySQL(MySQL):
    def __init__(self, db: str, driver: str = "mysqlconnector"):
        super().__init__(driver)
        ip = "localhost"
        port = 3306
        self.bindServer(ip, port, db)

class dbIO:

    def __init__(self, dbinf: baseDbInf):
        connStr = dbinf.getConnStr()
        self.placeholder = dbinf.argTempStr()
        self.engine = create_engine(connStr)
        self.DBSession = sessionmaker(bind = self.engine)

    @ contextlib.contextmanager
    def get_session(self, errormsg : str = "reason Unknown") -> Session:
        """return the session object to operate

        :param errormsg:
        :return: Session object

        example:
        >>> with self.get_session() as s:
        >>>     #do something here
        """
        session = self.DBSession()
        try:
            yield session
            session.commit()
        except Exception as e:
            session.rollback()
            logging.error("Error Occured: %s\n%s" % (e.args, errormsg))
        finally:
            session.close()

    def insert(self, table, record : dict) -> None:
        """insert a record into db

        :param table: table class
        :param record: dict of record to insert
        :return: None

        example:
        >>> x = {"fund_id" : "160023", "date" : "2005-09-02", "net_value" : 1.0102, "full_value" : 3.0102, "div" : 0.234, "pnl" : 0.2341}
        >>> dbIO.insert(MutualFundHist, x)
        """
        with self.get_session() as s:
            s.add(table(**record))

    def update(self, table, primary_kvs : dict, record : dict) -> None:
        """update record by looking at primary key

        :param table: table class
        :param primary_kvs: dict of primary key-value pairs, use to find which record(s) to update
        :param record: dict of new record
        :return: None

        example:
        >>> p = {"fund_id" : "150234", "date" : "2005-09-09"}  # use to find which records to update_all
        >>> r = {"net_value" : 0.4456}  # the new record to save to db
        >>> dbIO.update(MutualFundHist, p, r)
        is equivalent to:
        UPDATE MutualFundHist
        SET net_value = 0.4456
        WHERE fund_id = '150234' AND date = '2005-09-09';
        """
        with self.get_session() as s:
            #s.query(table).filter(table.fund_id == "150234").update_all(r)
            conditions = [getattr(table, k) == v for k, v in primary_kvs.items()]
            s.query(table).filter(and_(*conditions)).update_all(record)

    def delete(self, table, primary_kvs : dict) -> None:
        """delete record by looking at primary key

        :param table: table class
        :param primary_kvs: dict of primary key-value pairs, use to find which record(s) to update_all
        :return: None

        example:
        >>> p = {"fund_id" : "150234", "date" : "2005-09-09"}  # use to find which records to update_all
        >>> self.delete(MutualFundHist, p)
        is equivalent to:
        DELETE FROM MutualFundHist
        WHERE fund_id = '150234' AND date = '2005-09-09';
        """
        with self.get_session() as s:
            conditions = [getattr(table, k) == v for k, v in primary_kvs.items()]
            s.query(table).filter(and_(*conditions)).delete()


    def modify_sql(self, sql : str, *args, errormsg : str = "reason Unknown") -> None:
        """execute original sql (not query)

        :param sql: sql string
        :param args: arguments to fill in sql template
        :param errormsg: error message to display when error occurred
        :return: None

        example:
        >>> sql = 'UPDATE MutualFundHist SET net_value = %s WHERE fund_id = %s AND date = %s;'
        >>> self.modify_sql(sql, 0.7456, '150234', '2005-09-09')
        """
        with self.engine.connect() as conn:
            try:
                sql = sql.replace("?", self.placeholder)
                conn.execute(sql, *args)
            except Exception as e:
                logging.error("Error Occured: %s\n%s" % (e.args, errormsg))

    def query_df(self, query : Query) -> pd.DataFrame:
        """making query directly

        :param query: Query statement
        :return: resulting dataframe

        example:
        >>> with self.get_session() as s:
        >>>     query = s.query(MutualFundHist).filter(MutualFundHist.fund_id == "160023")
        >>>
        >>> df = self.query_df(query)
        """
        return pd.read_sql(query.statement, query.session.bind).replace({None : np.nan})

    def query_sql_df(self, sql : str, *args, errormsg : str = "reason Unknown") -> pd.DataFrame:
        """using original sql to make queries

        :param sql: the sql template
        :param args: the argument list to fill in the sql template
        :param errormsg: message to display when encoutering errors
        :return: the result dataframe

        example:
        >>> sql = "select * from mutualfundhist where fund_id = %s"
        >>> r = self.query_sql_df(sql, '160023')
        """
        with self.engine.connect() as conn:
            try:
                sql = sql.replace("?", self.placeholder)
                r = conn.execute(sql, *args)
                #df = pd.read_sql(sql, conn, params = args)
            except Exception as e:
                logging.error("Error Occured: %s\n%s" % (e.args, errormsg))
                df = pd.DataFrame({})
            else:
                headers = [ i[0] for i in r.cursor.description ]
                # df = pd.DataFrame.from_records(r, columns = headers)
                df = pd.DataFrame.from_records(r, columns = headers, coerce_float = True)
            finally:
                return df

    def insert_df(self, table, df : pd.DataFrame) -> None:
        """save a pandas table into database

        :param table: table class
        :param df: the pandas Dataframe to save into db
        :return: None

        example:
        >>> df = pd.DataFrame({
        >>>     "fund_id" : ["160023", "160023", "160023", "150234"],
        >>>     "date" : ["2005-09-09", "2005-09-02", "2005-08-31", "2005-09-09"],
        >>>     "net_value" : [1.0234, 1.0102, 1.0456, 0.9876],
        >>>     "full_value" : [3.0234, 3.0102, 3.0456, 2.9876],
        >>>     "div" : [None, 0.234, None, None],
        >>>     "split_ratio" : [1.0034, None, None, 1.2232],
        >>>     "pnl" : [0.2334, 0.2341, -0.1442, -0.0032],
        >>> })
        >>> self.insert_df(MutualFundHist, df)
        """
        records = df2Tables(df, table)

        with self.get_session() as s:
            s.add_all(records)

    def insert_pd_df(self, tablename: str, df : pd.DataFrame, errormsg : str = "reason Unknown") -> None:
        """save a pandas dataframe to database using pandas style

        :param tablename: the name str of the table
        :param df: the pandas dataframe to insert
        :return:
        """
        try:
            with self.engine.begin() as conn:  # .begin() will automatically tackle with rollback() and commit()
                df.to_sql(tablename, conn, if_exists = "append", index = False, chunksize = 1000)
        except Exception as e:
            logging.error("Error Occured: %s\n%s" % (e.args, errormsg))


    def insert_sql_df(self, tablename: str, df : pd.DataFrame, errormsg : str = "reason Unknown") -> None:
        """save a pandas dataframe to database using pythonic style

        :param tablename: the name str of the table
        :param df: the pandas dataframe to insert
        :return:
        """
        sql = f"""
        insert into {tablename} ({",".join(df.columns)})
        values ({",".join("?" for col in df.columns)})
        """
        sql = sql.replace("?", self.placeholder)

        conn = self.engine.raw_connection()
        cursor = conn.cursor()
        try:
            cursor.executemany(sql, df.values)
        except Exception as e:
            conn.rollback()
            logging.error("Error Occured: %s\n%s" % (e.args, errormsg))
        else:
            conn.commit()
        finally:
            cursor.close()
            conn.close()



def getPrimaryKeys(table) -> list:
    return [key.name for key in inspect(table).primary_key]

def df2Tables(df : pd.DataFrame, table) -> list:
    records = []
    for row in df.replace({np.nan: None}).to_dict(orient = "records"):
        records.append(table(**row))

    return records

# def tables2Df(tables : list) -> pd.DataFrame:
#     records = [t.to_dict() for t in tables]
#     return pd.DataFrame.from_records(records)


if __name__ == '__main__':


    username = input("Your DB username:\n")
    password = input("Your DB password:\n")

    lm = ScotiaDB2("DM1P1D")
    lm.login(username, password)
    db = dbIO(lm)

    # from utils import read_sql_from_file
    # from Helper_functions.db2_conn import db2_query


    sql = """
    SELECT DISTINCT 
        EFF_DT, RTL_RT AS EXCHG_RT 
    FROM 
        EDRDS.RPM_RT_FORGN_EXCH_RATES_SNAPSHOT 
    WHERE 
        FROM_CRNCY_CD = 'USD' 
    ORDER BY 
        EFF_DT
    """
    df = db.query_sql_df(sql)
    print(df)
    print(df.dtypes)